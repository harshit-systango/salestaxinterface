package Process;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.FileNotFoundException;
import Read.*;
import java.io.BufferedReader;
//import Writter.*;


public class processing implements Read {
   public List<String[]> process(final List<String[]> data) throws IOException ,FileNotFoundException,NumberFormatException
    {
        final List<String[]> newList = new ArrayList<>();
        for (final String[] row : data) 
        {
            final float price = Float.parseFloat(row[1]);
            final float per = Float.parseFloat(row[2]);
            final float salestax = price * per / 100;
            final float totalcost = price + salestax;
            final String[] newdata = { row[0], price + "", salestax + "", totalcost + "", row[3] };
            newList.add(newdata);
           // System.out.println("ss");


        }
        return newList;
    }

    public List<String[]> readFile(String FileName) throws IOException ,FileNotFoundException,NumberFormatException
    {
         String line = "";
        BufferedReader fileReader = null;
        fileReader = new BufferedReader(new FileReader(FileName));
        final List<String[]> data = new ArrayList<>();
        int counter = 0;
        while ((line = fileReader.readLine()) != null) 
        {
            if (counter != 0) {
                final String[] tokens = line.split(",");
                int arrLength=tokens.length;
                if(arrLength>4)
                 {
                     System.out.println("invalid data in csv file");
                     break;
                 }
                data.add(tokens);
            }
        counter++;
       // System.out.println(data);
    
        //System.out.println("d");
        
        }
        fileReader.close();
        return data;
    }

}
