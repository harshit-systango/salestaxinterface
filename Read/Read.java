package Read;

// import java.io.BufferedReader;
// import java.io.FileReader;
// import java.util.ArrayList;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.IOException;
//import Process.*;
//import Writter.*;
//import Sales.*;

public interface Read
{
    public List<String[]> readFile(String FileName) throws IOException ,FileNotFoundException,NumberFormatException;
}

